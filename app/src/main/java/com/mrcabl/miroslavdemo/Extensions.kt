package com.mrcabl.miroslavdemo

import android.content.Context
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.mrcabl.data.api.exceptions.ApiException
import com.mrcabl.data.api.exceptions.ApiHttpException

fun AppCompatActivity.showError(
    userErrorModel: UserErrorModel
) {
    showAlertDialog(
        this,
        userErrorModel.message
    )
}

fun showAlertDialog(context: Context, message: String) {
    val builder: AlertDialog.Builder = AlertDialog.Builder(context)
    builder.apply {
        setTitle(context.getString(R.string.info))
        setMessage(message)
        setCancelable(false)
        setPositiveButton(context.getString(R.string.ok)) { dialog, _ ->
            dialog.dismiss()
        }
        create().show()
    }
}


fun getUserFriendlyError(throwable: Throwable): UserErrorModel {
    val userErrorModel = UserErrorModel()
    if (throwable is ApiHttpException && throwable.demoError != null) {
        val htecErrors = throwable.demoError
        val message = htecErrors?.message

        if (message != null) {
            userErrorModel.message = message
        } else {
            userErrorModel.message = "Unknown error"
        }

    } else if (throwable is ApiException) {
        userErrorModel.message = throwable.errorMessage
    } else {
        throwable.message?.let {
            userErrorModel.message = it
        }
    }
    return userErrorModel
}