package com.mrcabl.miroslavdemo

import android.app.Application
import android.util.Log
import com.mrcabl.data.getErrorMessage
import com.mrcabl.domain.isNotNullOrEmpty
import com.mrcabl.miroslavdemo.di.DaggerDemoComponent
import com.mrcabl.miroslavdemo.di.DemoComponent
import com.mrcabl.miroslavdemo.di.DemoModule
import timber.log.Timber

class DemoApplication : Application() {

    @Suppress("unused")
    val demoComponent: DemoComponent by lazy {
        @Suppress("DEPRECATION")
        DaggerDemoComponent.builder()
            .demoModule(DemoModule(this))
            .build()
    }

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(ExtendedDebugTree())
        } else {
            Timber.plant(CrashReportingTree())
        }
    }

    class ExtendedDebugTree : Timber.DebugTree() {

        override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
            if (priority == Log.ERROR && t != null) {
                val exceptionMessage = t.getErrorMessage()
                if (exceptionMessage.isNotNullOrEmpty() && message.isNotNullOrEmpty()) {
                    super.log(
                        priority,
                        tag,
                        "Exception message: $exceptionMessage - From error_codes message: $message.",
                        t
                    )
                } else if (exceptionMessage.isNotNullOrEmpty()) {
                    super.log(priority, tag, "Exception message only: $exceptionMessage", t)
                } else {
                    super.log(priority, tag, message, t)
                }
            } else {
                super.log(priority, tag, message, t)
            }
        }
    }

    @Suppress("UNUSED_VALUE")
    class CrashReportingTree : Timber.Tree() {

        @Suppress("ASSIGNED_BUT_NEVER_ACCESSED_VARIABLE")
        override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
            if (priority == Log.ERROR && t != null) {
                val exceptionMessage = t.getErrorMessage()
                var logMessage: String? = null
                if (exceptionMessage.isNotNullOrEmpty() && message.isNotNullOrEmpty()) {
                    logMessage = "Exception message: $exceptionMessage - It happened: $message."
                } else if (exceptionMessage.isNotNullOrEmpty()) {
                    logMessage = "Exception message only: $exceptionMessage"
                }

                //TODO: Log to crashlytics
            }
        }
    }
}