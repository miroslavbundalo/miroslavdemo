package com.mrcabl.miroslavdemo.util


data class LiveDataTransfer<T>(val data: T? = null, val error: Throwable? = null)

fun <T> wrappedData(tFn: () -> T): LiveDataTransfer<T> = LiveDataTransfer(data = tFn())
fun <T> wrappedError(errFn: () -> Throwable): LiveDataTransfer<T> = LiveDataTransfer(error = errFn())