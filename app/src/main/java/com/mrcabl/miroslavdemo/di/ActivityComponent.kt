package com.mrcabl.miroslavdemo.di

import com.mrcabl.miroslavdemo.ui.home.HomeActivity
import dagger.Subcomponent

@Subcomponent(modules = [ActivityModule::class])
interface ActivityComponent {

    fun inject(homeActivity: HomeActivity)

    @Subcomponent.Builder
    interface Builder {
        fun withActivityModule(activityModule: ActivityModule): Builder

        fun build(): ActivityComponent
    }
}