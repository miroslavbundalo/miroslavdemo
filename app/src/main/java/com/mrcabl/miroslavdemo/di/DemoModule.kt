package com.mrcabl.miroslavdemo.di

import android.content.Context
import com.mrcabl.data.di.RoomModule
import com.mrcabl.domain.di.APP_CONTEXT
import com.mrcabl.miroslavdemo.DemoApplication
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module(
    includes = [AndroidModule::class, RoomModule::class],
    subcomponents = [ActivityComponent::class]
)
class DemoModule(private val demoApplication: DemoApplication) {

    @Provides
    @Named(APP_CONTEXT)
    fun providesAppContext(): Context = demoApplication.applicationContext
}