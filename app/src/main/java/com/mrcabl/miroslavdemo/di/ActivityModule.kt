package com.mrcabl.miroslavdemo.di

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.mrcabl.data.di.DataModule
import com.mrcabl.domain.di.ACTIVITY_CONTEXT
import com.mrcabl.domain.di.DomainModule
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module(
    includes = [ViewModelModule::class, DataModule::class, DomainModule::class]
)
class ActivityModule(private val activity: AppCompatActivity) {

    @Named(ACTIVITY_CONTEXT)
    @Provides
    fun provideActivityContext(): Context = activity
}