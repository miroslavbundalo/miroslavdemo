package com.mrcabl.miroslavdemo.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mrcabl.miroslavdemo.ui.ViewModelFactory
import com.mrcabl.miroslavdemo.ui.ViewModelKey
import com.mrcabl.miroslavdemo.ui.home.HomeViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    internal abstract fun mainActivityViewModel(viewModel: HomeViewModel): ViewModel


}