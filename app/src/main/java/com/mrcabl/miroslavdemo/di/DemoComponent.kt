package com.mrcabl.miroslavdemo.di

import dagger.Component

@Component(modules = [DemoModule::class])
interface DemoComponent {
    val activityBuilder: ActivityComponent.Builder
}