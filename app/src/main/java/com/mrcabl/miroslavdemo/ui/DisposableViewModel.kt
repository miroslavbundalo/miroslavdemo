package com.mrcabl.miroslavdemo.ui

import androidx.lifecycle.ViewModel

abstract class DisposableViewModel : ViewModel() {

    abstract fun disposeInteractors()

    override fun onCleared() {
        super.onCleared()
        disposeInteractors()
    }
}