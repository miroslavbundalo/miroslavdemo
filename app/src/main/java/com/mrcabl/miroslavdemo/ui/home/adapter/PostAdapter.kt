package com.mrcabl.miroslavdemo.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.mrcabl.domain.model.Post
import com.mrcabl.miroslavdemo.R
import com.mrcabl.miroslavdemo.databinding.ItemPostBinding

class PostAdapter(
    private val postList: ArrayList<Post>,
    private val onItemClick: (Post, Int) -> Unit
) :
    RecyclerView.Adapter<PostAdapter.PostViewHolder>() {

    fun updateList(posts: List<Post>) {
        postList.clear()
        postList.addAll(posts)
        notifyDataSetChanged()
    }

    fun removeItem(position: Int){
        postList.removeAt(position)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = DataBindingUtil.inflate<ItemPostBinding>(
            inflater,
            R.layout.item_post,
            parent,
            false
        )
        return PostViewHolder(view)
    }

    override fun getItemCount(): Int = postList.size


    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.view.post = postList[position]
        holder.bind(postList[position], position, onItemClick)
    }

    class PostViewHolder(var view: ItemPostBinding) : RecyclerView.ViewHolder(view.root) {
        fun bind(post: Post, position: Int, onItemClick: (Post, Int) -> Unit) {
            view.itemPostCv.setOnClickListener {
                onItemClick(post, position)
            }
        }
    }
}