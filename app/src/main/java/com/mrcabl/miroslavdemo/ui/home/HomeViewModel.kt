package com.mrcabl.miroslavdemo.ui.home

import androidx.lifecycle.MutableLiveData
import com.mrcabl.domain.executor.*
import com.mrcabl.domain.model.Post
import com.mrcabl.domain.model.User
import com.mrcabl.miroslavdemo.ui.DisposableViewModel
import com.mrcabl.miroslavdemo.util.LiveDataTransfer
import com.mrcabl.miroslavdemo.util.wrappedData
import com.mrcabl.miroslavdemo.util.wrappedError
import io.reactivex.functions.Action
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val getPosts: GetPosts,
    private val deletePost: DeletePost,
    private val getUser: GetUser
) : DisposableViewModel() {

    override fun disposeInteractors() {
        getPosts.dispose()
        deletePost.dispose()
        getUser.dispose()
    }

    init {
        getPosts()
    }

    val getPostsLiveData: MutableLiveData<LiveDataTransfer<List<Post>>> = MutableLiveData()
    val deletePostLiveData: MutableLiveData<LiveDataTransfer<Unit>> = MutableLiveData()
    val getUserLiveData: MutableLiveData<LiveDataTransfer<User>> = MutableLiveData()
    var selectedPost: Post? = null
    var selectedPostListPosition = -1


    fun deletePost(id: Int) {
        deletePost.execute(
            DeletePostParams(id),
            onSuccess = {
                deletePostLiveData.value = wrappedData { it }
            }, onError = {
                deletePostLiveData.value = wrappedError { it }
            }
        )
    }

    fun getUser(id: Int) {
        getUser.execute(
            GetUserParams(id),
            onSuccess = {
                getUserLiveData.value = wrappedData { it }
            }, onError = {
                getUserLiveData.value = wrappedError { it }
            }
        )
    }

    fun getPosts() {
        getPosts.execute(Unit,
            onNext = {
                getPostsLiveData.value = wrappedData { it }
            },
            onError = {
                getPostsLiveData.value = wrappedError { it }
            }, onCompleted = Action {
            }
        )
    }

}