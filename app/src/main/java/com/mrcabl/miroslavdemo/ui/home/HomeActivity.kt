package com.mrcabl.miroslavdemo.ui.home


import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mrcabl.domain.model.Post
import com.mrcabl.domain.model.User
import com.mrcabl.miroslavdemo.R
import com.mrcabl.miroslavdemo.databinding.ActivityHomeBinding
import com.mrcabl.miroslavdemo.di.ActivityComponent
import com.mrcabl.miroslavdemo.ui.BaseActivity
import com.mrcabl.miroslavdemo.ui.home.adapter.PostAdapter
import kotlinx.android.synthetic.main.activity_home.*
import javax.inject.Inject

class HomeActivity : BaseActivity() {

    override fun inject(injector: ActivityComponent) {
        injector.inject(this)
    }

    @Inject
    lateinit var viewModel: HomeViewModel
    private val postAdapter =
        PostAdapter(arrayListOf()) { post: Post, position: Int -> onPostClick(post, position) }
    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setCustomView(R.layout.custom_toolbar)
        supportActionBar?.customView?.findViewById<Button>(R.id.customToolbarBtnRefresh)
            ?.setOnClickListener {
                showLoader(true)
                viewModel.getPosts()
            }

        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        binding.viewModel = viewModel

        activityHomeRvPosts.layoutManager =
            LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        activityHomeRvPosts.adapter = postAdapter
        showLoader(true)

        viewModel.getPostsLiveData.observe(this, Observer {
            showLoader(false)
            if (it.data != null) {
                if (it.data.isEmpty()){
                    showNoPostsOverlay(findViewById(R.id.activityHomeClRoot))
                }else{
                    hideOverlay(findViewById(R.id.activityHomeClRoot))
                    postAdapter.updateList(it.data)
                }

            } else if (it.error != null) {
                handleError(it.error)
            }
        })

        viewModel.getUserLiveData.observe(this, Observer {
            showLoader(false)
            if (it.data != null) {
                showPostDialog(it.data, viewModel.selectedPost)
            } else if (it.error != null) {
                handleError(it.error)
            }
        })

        viewModel.deletePostLiveData.observe(this, Observer {
            showLoader(false)
            if (it.data != null) {
                postAdapter.removeItem(viewModel.selectedPostListPosition)
            } else if (it.error != null) {
                handleError(it.error)
            }
        })
    }

    private fun onPostClick(post: Post, position: Int) {
        showLoader(true)
        viewModel.selectedPost = post
        viewModel.selectedPostListPosition = position
        viewModel.getUser(post.userId)
    }

    private fun showPostDialog(user: User, post: Post?) {
        val inflater = layoutInflater
        val inflateView = inflater.inflate(R.layout.alert_dialog_user_details, null)
        val name = inflateView.findViewById<TextView>(R.id.alertDialogUserDetailsTvName)
        val email = inflateView.findViewById<TextView>(R.id.alertDialogUserDetailsTvEmail)

        name.text = user.name
        email.text = user.email

        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setTitle(getString(R.string.info))
        alertDialog.setView(inflateView)

        alertDialog.setPositiveButton(getString(R.string.delete)) { dialog, _ ->
            post?.let { viewModel.deletePost(it.id) }
            dialog.dismiss()
            showLoader(true)
        }
        alertDialog.setCancelable(false)
        alertDialog.setNegativeButton(getString(R.string.close)) { dialog, _ -> dialog.dismiss() }
        val dialog = alertDialog.create()
        dialog.show()
    }
}