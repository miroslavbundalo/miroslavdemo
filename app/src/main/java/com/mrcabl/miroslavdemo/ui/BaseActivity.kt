@file:Suppress("DEPRECATION")

package com.mrcabl.miroslavdemo.ui

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import com.mrcabl.miroslavdemo.DemoApplication
import com.mrcabl.miroslavdemo.R
import com.mrcabl.miroslavdemo.di.ActivityComponent
import com.mrcabl.miroslavdemo.di.ActivityModule
import com.mrcabl.miroslavdemo.getUserFriendlyError
import com.mrcabl.miroslavdemo.showError

abstract class BaseActivity : AppCompatActivity() {

    @Suppress("DEPRECATION")
    private lateinit var progressDialog: ProgressDialog
    protected var overlayView: View? = null
    val injector: ActivityComponent by lazy {
        val demoApplication = applicationContext as DemoApplication
        val activityModule = ActivityModule(this@BaseActivity)
        demoApplication.demoComponent.activityBuilder.withActivityModule(
            activityModule
        ).build()
    }

    abstract fun inject(injector: ActivityComponent)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject(injector)
    }

    protected fun handleError(error: Throwable) {
        showError(getUserFriendlyError(error))
    }

    protected fun hideOverlay(view: View) {
        // remove current overlay if there is any
        if (overlayView != null) {
            (view as ViewGroup).removeView(overlayView)
            overlayView = null
        }
    }

    protected fun showNoPostsOverlay(view: View) {
        // remove current overlay if there is any
        if (overlayView != null) {
            (view as ViewGroup).removeView(overlayView)
            overlayView = null
        }

        // show loading overlay
        val layoutInflater = LayoutInflater.from(this)
        val loadingView =
            layoutInflater.inflate(R.layout.empty_screen, view as ViewGroup, false)
        view.addView(loadingView)

        overlayView = loadingView
    }

    protected fun showLoader(show: Boolean) {
        if (show) {
            if (::progressDialog.isInitialized)
                progressDialog.dismiss()
            @Suppress("DEPRECATION")
            progressDialog = ProgressDialog(this, R.style.NewDialog)
            progressDialog.show()
            progressDialog.setCancelable(false)
            @Suppress("DEPRECATION")
            progressDialog.isIndeterminate = true
        } else {
            if (::progressDialog.isInitialized)
                progressDialog.dismiss()
        }
    }
}