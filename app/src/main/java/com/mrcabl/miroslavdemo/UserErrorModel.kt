package com.mrcabl.miroslavdemo

import com.mrcabl.domain.EMPTY_STRING

data class UserErrorModel(
    var message: String = EMPTY_STRING
) {
    @Suppress("unused")
    fun isNotNoShow(): Boolean = message.isNotEmpty()
}