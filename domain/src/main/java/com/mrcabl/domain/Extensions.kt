package com.mrcabl.domain

fun String?.isNotNullOrEmpty() = !isNullOrEmpty()