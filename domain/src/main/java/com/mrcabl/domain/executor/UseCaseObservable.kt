package com.mrcabl.domain.executor

import com.mrcabl.domain.RxDisposable
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer

abstract class UseCaseObservable<RESULT, in PARAMS>(
    private val executionScheduler: Scheduler,
    private val postExecutionScheduler: Scheduler
) : RxDisposable {

    override var disposables: CompositeDisposable = CompositeDisposable()

    fun execute(
        params: PARAMS,
        onNext:  (RESULT) -> Unit,
        onError: (Throwable) -> Unit,
        onCompleted: (Action)
    ) {
        disposable {
            build(params)
                .subscribeOn(executionScheduler)
                .observeOn(postExecutionScheduler, true)
                .subscribe(Consumer(onNext), Consumer(onError), onCompleted)
        }
    }

    abstract fun build(params: PARAMS): Observable<RESULT>
}