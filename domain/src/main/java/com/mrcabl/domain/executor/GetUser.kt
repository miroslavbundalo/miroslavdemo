package com.mrcabl.domain.executor

import com.mrcabl.domain.model.User
import com.mrcabl.domain.repositories.UserRepository
import io.reactivex.Scheduler
import io.reactivex.Single

class GetUser(
    private val userRepository: UserRepository,
    executeScheduler: Scheduler,
    postExecuteScheduler: Scheduler
) : UseCase<User, GetUserParams>(executeScheduler, postExecuteScheduler){
    override fun build(params: GetUserParams): Single<User> {
        return userRepository.getUser(params.id)
    }
}

data class GetUserParams(val id: Int)