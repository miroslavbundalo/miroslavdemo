package com.mrcabl.domain.executor

import com.mrcabl.domain.repositories.PostLocalStorageRepository
import io.reactivex.Scheduler
import io.reactivex.Single

class ClearPostsFromLocalStorage(
    private val postLocalStorageRepository: PostLocalStorageRepository,
    executeScheduler: Scheduler,
    postExecuteScheduler: Scheduler
) : UseCase<Unit, Unit>(executeScheduler, postExecuteScheduler) {
    override fun build(params: Unit): Single<Unit> = postLocalStorageRepository.clearPostsFromLocalStorage()
}