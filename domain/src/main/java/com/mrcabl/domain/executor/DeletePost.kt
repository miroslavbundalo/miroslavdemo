package com.mrcabl.domain.executor

import com.mrcabl.domain.repositories.PostLocalStorageRepository
import io.reactivex.Scheduler
import io.reactivex.Single

class DeletePost(
    private val postLocalStorageRepository : PostLocalStorageRepository,
    executeScheduler: Scheduler,
    postExecuteScheduler: Scheduler
) : UseCase<Unit, DeletePostParams>(executeScheduler, postExecuteScheduler) {
    override fun build(params: DeletePostParams): Single<Unit> =
        postLocalStorageRepository.deletePostFromLocalStorage(params.id)
}

data class DeletePostParams(val id: Int)