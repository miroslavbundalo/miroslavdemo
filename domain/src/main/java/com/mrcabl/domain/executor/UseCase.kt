package com.mrcabl.domain.executor

import com.mrcabl.domain.RxDisposable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable

abstract class UseCase<RESULT, in PARAMS>(
    private val executionScheduler: Scheduler,
    private val postExecutionScheduler: Scheduler
) : RxDisposable {

    override var disposables: CompositeDisposable = CompositeDisposable()

    fun execute(
        params: PARAMS,
        onSuccess: (RESULT) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        disposable {
            build(params)
                .subscribeOn(executionScheduler)
                .observeOn(postExecutionScheduler)
                .subscribe(onSuccess, onError)
        }
    }

    abstract fun build(params: PARAMS): Single<RESULT>
}