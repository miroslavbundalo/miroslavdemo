package com.mrcabl.domain.executor

import com.mrcabl.domain.model.Post
import com.mrcabl.domain.repositories.PostLocalStorageRepository
import com.mrcabl.domain.repositories.PostRepository
import io.reactivex.Observable
import io.reactivex.Scheduler

class GetPosts(
    private val postRepository: PostRepository,
    private val postLocalStorageRepository: PostLocalStorageRepository,
    executeScheduler: Scheduler,
    postExecuteScheduler: Scheduler
) : UseCaseObservable<List<Post>, Unit>(executeScheduler, postExecuteScheduler) {
    override fun build(params: Unit): Observable<List<Post>> {
        return Observable.concat(
            postLocalStorageRepository.getPostsFromLocalStorage().toObservable(),
            postRepository.getPostsFromApi().toObservable()
        )
    }
}