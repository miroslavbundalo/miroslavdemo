package com.mrcabl.domain.model

data class Post(
    val id: Int,
    val userId: Int,
    val title: String,
    val body: String,
    val startValidityTime: String?,
    val endValidityTime: String?
)