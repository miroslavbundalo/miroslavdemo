package com.mrcabl.domain.model

data class User(
    val name: String,
    val email: String
)