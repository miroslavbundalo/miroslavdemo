package com.mrcabl.domain.di

import com.mrcabl.domain.executor.*
import com.mrcabl.domain.repositories.PostLocalStorageRepository
import com.mrcabl.domain.repositories.PostRepository
import com.mrcabl.domain.repositories.UserRepository
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

@Module
class DomainModule {

    @Provides
    fun providesClearPostsFromLocalStorage(postLocalStorageRepository: PostLocalStorageRepository) =
        ClearPostsFromLocalStorage(postLocalStorageRepository, Schedulers.io(), AndroidSchedulers.mainThread())

    @Provides
    fun providesDeletePost(postLocalStorageRepository : PostLocalStorageRepository) =
        DeletePost(postLocalStorageRepository, Schedulers.io(), AndroidSchedulers.mainThread())

    @Provides
    fun providesGetPosts(postRepository: PostRepository, postLocalStorageRepository: PostLocalStorageRepository) =
        GetPosts(postRepository,postLocalStorageRepository, Schedulers.io(), AndroidSchedulers.mainThread())

    @Provides
    fun providesGetUser(userRepository: UserRepository) =
        GetUser(userRepository, Schedulers.io(), AndroidSchedulers.mainThread())
}