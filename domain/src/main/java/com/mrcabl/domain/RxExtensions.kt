package com.mrcabl.domain

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

interface RxDisposable {

    var disposables: CompositeDisposable

    fun disposable(disposableFn: () -> Disposable) {
        if (disposables.isDisposed) {
            disposables = CompositeDisposable()
        }

        val disposable = disposableFn()
        disposables.add(disposable)
    }

    fun dispose() {
        if (!disposables.isDisposed) {
            disposables.dispose()
        }
    }
}