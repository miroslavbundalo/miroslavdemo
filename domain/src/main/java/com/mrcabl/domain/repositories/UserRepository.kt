package com.mrcabl.domain.repositories

import com.mrcabl.domain.model.User
import io.reactivex.Single

interface UserRepository {
    fun getUser(userId: Int): Single<User>
}