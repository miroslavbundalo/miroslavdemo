package com.mrcabl.domain.repositories

import com.mrcabl.domain.model.Post
import io.reactivex.Single

interface PostLocalStorageRepository {
    fun getPostsFromLocalStorage(): Single<List<Post>>
    fun deletePostFromLocalStorage(id: Int): Single<Unit>
    fun storePostsToLocalStorage(posts: List<Post>): Single<Unit>
    fun clearPostsFromLocalStorage(): Single<Unit>
}