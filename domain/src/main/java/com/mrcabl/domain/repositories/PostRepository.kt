package com.mrcabl.domain.repositories

import com.mrcabl.domain.model.Post
import io.reactivex.Observable
import io.reactivex.Single

interface PostRepository {
    fun getPostsFromApi(): Single<List<Post>>
}