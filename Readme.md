# Project Title
Miroslav Bundalo Demo

## Short Description
This is Andtrod Demo App

Here is demonstrated MVVM architecture using modular approach

**modules** 
- *app*
- *data*
- *domain*


## Prerequisites
Minimum version of android SDK 21
## Deployment

App have one flavor dimension "demo" and one buildType "debug"

release buildType is excluded from build because this app is for demo

## Frameworks Used
* [Kotlin](https://kotlinlang.org/)
* [RXJava](https://github.com/ReactiveX/RxAndroid)
* [Room](https://developer.android.com/topic/libraries/architecture/room)
* [Data Binding](https://developer.android.com/topic/libraries/data-binding)
* [Retrofit](https://square.github.io/retrofit/)
* [OkHttp3](https://github.com/square/okhttp)
* [Dagger2](https://developer.android.com/training/dependency-injection/dagger-android)
* [LiveData](https://developer.android.com/topic/libraries/architecture/livedata)
* [GSON](https://github.com/google/gson)

## Contributing
- **master branch** - main branch

branch flow: 
development -> master

## Versioning

example: 1.20.24.00
1 - app version
20 - current year
24 - current week number
00 - number of iterations in current week

## Authors

* **Miroslav Bundalo**


## License

This project is property of Miroslav Bundalo

