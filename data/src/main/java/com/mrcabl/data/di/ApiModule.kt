package com.mrcabl.data.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.mrcabl.data.api.Api
import com.mrcabl.data.api.ApiErrorHandler
import dagger.Module
import dagger.Provides

@Module
class ApiModule {

    @Provides
    fun providesGson(): Gson = GsonBuilder().create()

    @Provides
    fun providesRxErrorHandler(): ApiErrorHandler = ApiErrorHandler()

    @Provides
    fun providesApi(gson: Gson): Api = Api(gson)

    @Provides
    fun providesPostApi(api: Api) = api.getPostApi()

    @Provides
    fun providesUserApi(api: Api) = api.getUserApi()

}