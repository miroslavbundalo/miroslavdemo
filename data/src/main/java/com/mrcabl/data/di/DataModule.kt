package com.mrcabl.data.di

import dagger.Module

@Module(includes = [RepositoryModule::class, ApiModule::class])
class DataModule