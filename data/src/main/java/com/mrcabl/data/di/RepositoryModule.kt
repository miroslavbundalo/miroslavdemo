package com.mrcabl.data.di

import com.mrcabl.data.api.ApiErrorHandler
import com.mrcabl.data.api.PostApi
import com.mrcabl.data.api.UserApi
import com.mrcabl.data.dao.PostDao
import com.mrcabl.data.repository.ApiPostRepository
import com.mrcabl.data.repository.ApiUserRepository
import com.mrcabl.data.repository.DbPostRepository
import com.mrcabl.domain.repositories.PostLocalStorageRepository
import com.mrcabl.domain.repositories.PostRepository
import com.mrcabl.domain.repositories.UserRepository
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {

    @Provides
    fun provideApiPostRepository(
        apiErrorHandler: ApiErrorHandler,
        postApi: PostApi,
        postLocalStorageRepository: PostLocalStorageRepository
    ): PostRepository {
        return ApiPostRepository(apiErrorHandler, postApi, postLocalStorageRepository )
    }

    @Provides
    fun provideApiUserRepository(
        apiErrorHandler: ApiErrorHandler,
        userApi: UserApi
    ): UserRepository {
        return ApiUserRepository(apiErrorHandler, userApi)
    }

    @Provides
    fun provideDbPostRepository(postDao: PostDao): PostLocalStorageRepository =
        DbPostRepository(postDao)
}