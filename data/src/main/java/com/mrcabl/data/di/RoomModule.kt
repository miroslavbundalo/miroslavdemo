package com.mrcabl.data.di

import android.content.Context
import androidx.room.Room
import com.mrcabl.data.dao.PostDao
import com.mrcabl.data.db.DemoDataBase
import com.mrcabl.domain.di.APP_CONTEXT
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class RoomModule {

    @Provides
    fun provideMiroslavHtecTestDataBase(@Named(APP_CONTEXT) applicationContext: Context): DemoDataBase {
        return Room.databaseBuilder(
            applicationContext,
            DemoDataBase::class.java,
            "database.db"
        ).build()
    }

    @Provides
    fun providePostDao(demoDataBase: DemoDataBase): PostDao =
        demoDataBase.getPostDao()
}