package com.mrcabl.data.dao

import androidx.room.*
import com.mrcabl.data.model.entity.PostDBEntity
import io.reactivex.Single

@Dao
interface PostDao {

    @Transaction
    @Query("SELECT * FROM Post")
    fun getPosts(): Single<List<PostDBEntity>>

    @Transaction
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(posts: List<PostDBEntity>): Single<Unit>

    @Transaction
    @Query("DELETE FROM Post")
    fun deleteAll(): Single<Unit>

    @Query("DELETE FROM Post WHERE id = :id")
    fun delete(id: Int): Single<Unit>

    @Insert
    fun insert(postDBEntity: PostDBEntity)

}