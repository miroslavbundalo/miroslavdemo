package com.mrcabl.data

import android.annotation.SuppressLint
import com.mrcabl.data.api.exceptions.ApiException
import io.reactivex.exceptions.CompositeException
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


fun Throwable.getErrorMessage(): String? {
    return when (this) {
        is ApiException -> errorMessage
        is CompositeException -> exceptions[0].message
        else -> message
    }
}

@SuppressLint("SimpleDateFormat")
fun String.convertMillisecondsToUserFriendlyTime() : String{
    val date = Date(this.toLong())
    val formatter: DateFormat = SimpleDateFormat("dd.MM.yyyy HH:mm")
    formatter.timeZone = TimeZone.getDefault()
    return formatter.format(date)
}