package com.mrcabl.data.model

const val SOME_ERROR_CODE = 1000 // for example app must be updated
const val SOME_ERROR_FOR_RETRY_NETWORK_CALL = 1001 // for example refresh token