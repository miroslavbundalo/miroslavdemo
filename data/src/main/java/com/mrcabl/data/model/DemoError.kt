package com.mrcabl.data.model

data class DemoError(val success: Boolean, val message: String, val code: Int)