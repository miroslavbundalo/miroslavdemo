package com.mrcabl.data.model.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.mrcabl.data.FIVE_MIN_IN_MILIS
import java.util.*

@Entity(tableName = "Post")
data class PostDBEntity(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: Int,
    @ColumnInfo(name = "userId") val userId: Int,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "body") val body: String,
    @ColumnInfo(name = "start_validity_time") val startValidityTime: String = Date().time.toString(),
    @ColumnInfo(name = "end_validity_time") val endValidityTime: String = (Date().time + FIVE_MIN_IN_MILIS).toString()
)