package com.mrcabl.data.api

import com.mrcabl.data.api.exceptions.ApiHttpException
import com.mrcabl.data.model.SOME_ERROR_CODE
import com.mrcabl.data.model.SOME_ERROR_FOR_RETRY_NETWORK_CALL
import io.reactivex.Flowable
import io.reactivex.SingleTransformer
import timber.log.Timber

class ApiErrorHandler {

    fun <T> interceptErrors(): SingleTransformer<T, T> = SingleTransformer { upstream ->
        upstream
            .doOnError { throwable ->
                if (throwable is ApiHttpException && throwable.demoError != null) {
                    val htecError = throwable.demoError

                    when (htecError.code) {
                        SOME_ERROR_CODE -> {
                            /*TODO Ping RxRouter to show update dialog*/
                        }

                        else -> {
                            // do nothing
                        }
                    }
                }

                Timber.e(throwable)
            }
            .retryWhen { errors ->
                errors.flatMap { throwable ->
                    val interceptedPublisher: Flowable<*> = Flowable.error<Throwable>(throwable)

                    if (throwable is ApiHttpException && throwable.demoError != null) {
                        val htecError = throwable.demoError
                        @Suppress("ControlFlowWithEmptyBody")
                        if (htecError.code == SOME_ERROR_FOR_RETRY_NETWORK_CALL) {
//                                    interceptedPublisher = loginRepository.tokenRefresh()
//                                        .doOnSuccess {  }
//                                        .doOnError {  }
//                                        .toFlowable()
                        }
                    }

                    interceptedPublisher
                }
            }
    }
}