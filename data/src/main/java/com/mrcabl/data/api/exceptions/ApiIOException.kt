package com.mrcabl.data.api.exceptions

class ApiIOException(exception: Exception) : ApiException() {
    init {
        errorMessage = exception.message ?: "There is no internet connection"

    }
}