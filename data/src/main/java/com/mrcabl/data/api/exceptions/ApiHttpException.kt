package com.mrcabl.data.api.exceptions

import com.mrcabl.data.model.DemoError
import retrofit2.Response
import retrofit2.Retrofit

class ApiHttpException(
    response: Response<*>,
    retrofit: Retrofit
) : ApiException() {

    private val converter = retrofit.responseBodyConverter<DemoError>(
        DemoError::class.java,
        arrayOfNulls<Annotation>(0)
    )

    val demoError: DemoError?

    init {
        demoError = response.errorBody()?.let { converter.convert(it) }
        errorMessage =
            demoError?.message.toString()
    }
}