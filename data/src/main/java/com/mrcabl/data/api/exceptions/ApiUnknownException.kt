package com.mrcabl.data.api.exceptions

class ApiUnknownException(throwable: Throwable) : ApiException() {
    init {
        errorMessage = throwable.message ?: "Unknown veennerErrors occurred"
    }
}