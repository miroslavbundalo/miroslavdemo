package com.mrcabl.data.api.exceptions

open class ApiException : Throwable() {

    lateinit var errorMessage: String
}