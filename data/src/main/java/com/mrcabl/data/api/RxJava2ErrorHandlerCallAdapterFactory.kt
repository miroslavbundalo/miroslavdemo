package com.mrcabl.data.api

import com.mrcabl.data.api.exceptions.*
import com.mrcabl.data.model.DemoError
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.Function
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.io.IOException
import java.lang.reflect.Type

class RxJava2ErrorHandlerCallAdapterFactory : CallAdapter.Factory() {

    private val original: RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.create()

    override fun get(
        returnType: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): CallAdapter<*, *>? {
        return RxJava2ErrorAdapterWrapper(retrofit, original.get(returnType, annotations, retrofit))
    }

    class RxJava2ErrorAdapterWrapper<R>(
        val retrofit: Retrofit,
        private val wrapped: CallAdapter<R, *>?
    ) :
        CallAdapter<R, Any> {

        override fun responseType(): Type = wrapped?.responseType() ?: DemoError::class.java

        override fun adapt(call: Call<R>): Any {
            return wrapped?.let {
                val adoptedCall = it.adapt(call)
                when (adoptedCall) {
                    is Single<*> -> {
                        adoptedCall.onErrorResumeNext { throwable ->
                            val apiException = apiException(throwable)
                            Single.error(apiException)
                        }
                    }
                    is Observable<*> -> adoptedCall.onErrorResumeNext(Function { throwable ->
                        val apiException = apiException(throwable)
                        Observable.error(apiException)
                    })
                    else -> adoptedCall
                }

            } ?: Single.error<InternalRxJavaErrorHandlerException>(
                InternalRxJavaErrorHandlerException()
            )
        }

        private fun apiException(throwable: Throwable): ApiException? {
            return when (throwable) {
                is HttpException -> throwable.response()
                    ?.let { it1 -> ApiHttpException(it1, retrofit) }
                is IOException -> ApiIOException(throwable)
                else -> ApiUnknownException(throwable)
            }
        }

    }

}