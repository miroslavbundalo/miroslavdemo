package com.mrcabl.data.api

import com.mrcabl.domain.model.User
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface UserApi {

    @GET("/users/{userId}")
    fun getUser(@Path("userId") userId: Int) : Single<User>
}