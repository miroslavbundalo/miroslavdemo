package com.mrcabl.data.api

import com.mrcabl.domain.model.Post
import io.reactivex.Single
import retrofit2.http.GET

interface PostApi {

    @GET("/posts")
    fun getPosts(): Single<List<Post>>
}