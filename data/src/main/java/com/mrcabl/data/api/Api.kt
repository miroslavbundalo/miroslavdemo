package com.mrcabl.data.api

import com.google.gson.Gson
import com.mrcabl.data.BuildConfig.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

open class Api(gson: Gson) {

    private val apiUrl = "$protocol$domain"

    protected val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(apiUrl)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2ErrorHandlerCallAdapterFactory())
        .client(getHttpClient())
        .build()

    private fun getHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor {
                val requestBuilder = it.request().newBuilder()

                requestBuilder.addHeader("Content-Type", "application/json")

                val request = requestBuilder.build()
                it.proceed(request)
            }
            .addInterceptor(
                HttpLoggingInterceptor().setLevel(
                    if (DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
                )
            )
            .build()
    }

    fun getPostApi(): PostApi = retrofit.create(PostApi::class.java)
    fun getUserApi(): UserApi = retrofit.create(UserApi::class.java)
}