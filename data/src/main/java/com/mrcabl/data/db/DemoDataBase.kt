package com.mrcabl.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.mrcabl.data.dao.PostDao
import com.mrcabl.data.model.entity.PostDBEntity

@Database(entities = [(PostDBEntity::class)], version = 1)
abstract class DemoDataBase : RoomDatabase() {
    abstract fun getPostDao(): PostDao
}