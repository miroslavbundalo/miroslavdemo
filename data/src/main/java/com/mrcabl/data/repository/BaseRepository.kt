package com.mrcabl.data.repository

import com.mrcabl.data.api.ApiErrorHandler
import io.reactivex.Single

open class BaseRepository(private val apiErrorHandler: ApiErrorHandler) {

    protected fun <T> Single<T>.handleApiErrors(): Single<T> {
        return this.compose(apiErrorHandler.interceptErrors())
    }
}