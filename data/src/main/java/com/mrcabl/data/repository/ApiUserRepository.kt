package com.mrcabl.data.repository

import com.mrcabl.data.api.ApiErrorHandler
import com.mrcabl.data.api.UserApi
import com.mrcabl.domain.model.User
import com.mrcabl.domain.repositories.UserRepository
import io.reactivex.Single

class ApiUserRepository(apiErrorHandler: ApiErrorHandler, private val userApi: UserApi) :
    BaseRepository(apiErrorHandler), UserRepository {

    override fun getUser(userId: Int): Single<User> {
        return userApi.getUser(userId).handleApiErrors()
    }
}