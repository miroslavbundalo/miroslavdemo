package com.mrcabl.data.repository

import com.mrcabl.data.api.ApiErrorHandler
import com.mrcabl.data.api.PostApi
import com.mrcabl.domain.model.Post
import com.mrcabl.domain.repositories.PostLocalStorageRepository
import com.mrcabl.domain.repositories.PostRepository
import io.reactivex.Single

class ApiPostRepository(
    apiErrorHandler: ApiErrorHandler,
    private val postApi: PostApi,
    private val postLocalStorageRepository: PostLocalStorageRepository
) : BaseRepository(apiErrorHandler), PostRepository {

    override fun getPostsFromApi(): Single<List<Post>> {
        return postApi.getPosts().flatMap { post ->
            postLocalStorageRepository.storePostsToLocalStorage(post).flatMap {
                postLocalStorageRepository.getPostsFromLocalStorage()
            }
        }
    }
}