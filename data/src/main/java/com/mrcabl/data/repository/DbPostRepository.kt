package com.mrcabl.data.repository

import com.mrcabl.data.convertMillisecondsToUserFriendlyTime
import com.mrcabl.data.dao.PostDao
import com.mrcabl.data.model.entity.PostDBEntity
import com.mrcabl.domain.model.Post
import com.mrcabl.domain.repositories.PostLocalStorageRepository
import io.reactivex.Observable
import io.reactivex.Single

class DbPostRepository(private val postDao: PostDao) : PostLocalStorageRepository {

    override fun getPostsFromLocalStorage(): Single<List<Post>> {
        return postDao.getPosts().flatMap { list ->

                val posts = mutableListOf<Post>()
                list.forEach {
                    posts.add(
                        Post(
                            it.id,
                            it.userId,
                            it.title,
                            it.body,
                            it.startValidityTime.convertMillisecondsToUserFriendlyTime(),
                            it.endValidityTime.convertMillisecondsToUserFriendlyTime()
                        )
                    )
                }
                Single.just(posts)
        }
    }

    override fun deletePostFromLocalStorage(id: Int): Single<Unit> {
        return postDao.delete(id)
    }

    override fun storePostsToLocalStorage(posts: List<Post>): Single<Unit> {
        val postDBEntity = mutableListOf<PostDBEntity>()
        posts.forEach {
            postDBEntity.add(PostDBEntity(it.id, it.userId, it.title, it.body))
        }
        return postDao.insertAll(postDBEntity)
    }

    override fun clearPostsFromLocalStorage(): Single<Unit> {
        return postDao.deleteAll()
    }
}